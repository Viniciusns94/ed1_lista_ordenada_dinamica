/*---------------------------------------------------------------------------------------------------
autor: viniciusns94
29/04/2016 
Função insere:
1. Memória cheia = lista cheia!
2. Inserir uma informação no começo da lista
3. Inserir uma informação no meio ou fim da lista

Função remoção:
1. Lista vazia
2. Valor não encontrado
3. Remover no início lista
	[]-> [A][]-> [B][]->...   
4. Remover do meio ou fim da lista
---------------------------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>

typedef struct no{
	int info;
	struct no *prox;	
} *ptno;

ptno insere(ptno L, int valor){
	ptno p = (ptno) malloc (sizeof(struct no));	
	if(!p) 
		puts("Memória cheia");
	else{
		ptno ant = NULL, aux = L;
		while(aux && valor > aux->info){ //ordena a lista
			ant = aux;
			aux = aux->prox;
		}
		aux = p;
		aux->info = valor;		
		if(!ant){
			aux->prox = L;
			L = aux;
		}
		else{
			aux->prox = ant->prox;
			ant->prox = aux;
		}
	}
	return L;
}

void mostra(ptno L){ 
	while(L){
		printf("%d ", L->info);
		L=L->prox;
	}
}

int soma(ptno L){
	int s = 0;
	while(L){
		s += L->info;
		L = L->prox;
	}
	return s;
}
/*---------------------------------------------------------------------------------------------------------------------------------------------
Ex.(7)-> Dada uma lista L e um vetor X codifique a rotina OCORRE(L,X) q retorna o nº de vezes q o valor X ocorre na lista
---------------------------------------------------------------------------------------------------------------------------------------------*/
int ocorre(ptno L, int valor){
	int cont=0;
	while(L && valor >= L->info){
		if(L->info == valor)
			cont++;
		L = L->prox;
	}
	return cont;
}

ptno inverte(ptno L){
	ptno Q = L, P = NULL;
	while(Q){
		Q = Q->prox;
		L->prox = P;
		P = L;
		L = Q;
	}
	return P;
}

ptno retira(ptno L, int valor){
	if(!L)
		puts("Lista vazia!!");//caso 1
	else{
		ptno aux = L, ant = NULL;
		while(aux && valor > aux->info){
			ant = aux;
			aux = aux->prox;
		}
		if(!aux || valor < aux->info)//caso 2
			puts("Valor não encontrado, valor informado não está na lista");
		else{
			if(valor == L->info)//caso 3 //aux->info faz os valores serem removidos do início até o valor indicado L->info remove apenas o valor desejado
				L = aux->prox;
			else //caso 4
				ant->prox = aux->prox;
			free(aux);			
		}
	}
	return L;
}
/*---------------------------------------------------------------------------------------------------------------------------------------------
Informa o tamanho da lista, contanto a quantidade de elementos de tal
---------------------------------------------------------------------------------------------------------------------------------------------*/
int tamanho(ptno L){
	int conta = 0;
	while(L){
		conta ++;
		L = L->prox;
	}
	return conta;
}

void main(){
	printf("\t******* Lista Ordenada por Alocação Dinâmica de Nós *******\n");

	ptno L = NULL;
	int num;
	puts("\nAVISO: Digite 0 para finalizar a insersão");
	printf("\nInserir valores\n>");
	do{
		scanf("%d", &num);
		if(num){
			L = insere(L, num);
			printf("Lista: ");
			mostra(L);
			printf("\n> ");
		}
	}
	while(num);
	printf("\nSoma da Lista = %d\n", soma(L));
	printf("Ocorrem [%d] valores 5 na lista\n", ocorre(L, 5));

	L = inverte(L);
	puts("\nLista invertida");
	mostra(L);
	L = inverte(L);
	puts("\nLista desinvertida");
	mostra(L);	

	puts("\n\nAVISO: Digite 0 para finalizar o programa");
	puts("\nRemover valores");
	do{
		scanf("%d", &num);
		if(num){
			L = retira(L, num);
			printf("Lista: ");
			mostra(L);
			printf("\n> ");
		}
	}while(num);
	printf("\nSoma Final da Lista = %d\n", soma(L));

	printf("Tamanho final da lista: %d\nFIM\n", tamanho(L));
}